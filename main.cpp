#include <mutex>
#include <vector>
#include <chrono>
#include <thread>

#include "ai.h"
#include "cam.h"
#include "common.h"
#include "navigation.h"

void run_cam(std::vector<unsigned int>& my_image, std::mutex& m, bool& done_cam)
{
    while (1) 
    {
        auto start_time = std::chrono::steady_clock::now();
        cam::run(my_image, m, done_cam);
        auto end_time = std::chrono::steady_clock::now();
        std::this_thread::sleep_until(start_time + std::chrono::milliseconds(50));
    }
}

void run_nav(std::vector<unsigned int>& my_image, std::mutex& m, bool& done_nav)
{
    while (1)
    {
        auto start_time = std::chrono::steady_clock::now();
        navigation::run(my_image, m, done_nav);
        auto end_time = std::chrono::steady_clock::now();
        std::this_thread::sleep_until(start_time + std::chrono::milliseconds(100));
    }
}

void run_ai(std::vector<unsigned int>& my_image, std::mutex& m, bool& done_ai)
{
    while (1)
    {
        auto start_time = std::chrono::steady_clock::now();
        ai::run(my_image, m, done_ai);
        auto end_time = std::chrono::steady_clock::now();
        std::this_thread::sleep_until(start_time + std::chrono::milliseconds(200));
    }
}

int main()
{
    const unsigned int width = 1920;
    const unsigned int height = 1280;
    std::vector<unsigned int> my_image(width * height, 1);

    std::mutex m;
    bool done_cam = false, done_ai = false, done_nav = false;

    std::thread t_cam(run_cam, std::ref(my_image), std::ref(m), std::ref(done_cam));
    std::thread t_nav(run_nav, std::ref(my_image), std::ref(m), std::ref(done_nav));
    std::thread t_ai(run_ai, std::ref(my_image), std::ref(m), std::ref(done_ai));

    t_cam.join();
    t_nav.join();
    t_ai.join();

    return 0;
}


